#!/usr/bin/python
import time
import sys
import numpy as np

import serial

try:
    from sklearn import neighbors, svm
    HAVE_SK = True
except ImportError:
    HAVE_SK = False

import myo

#Initialize Myo object
m = myo.Myo(myo.NNClassifier(), sys.argv[1] if len(sys.argv) >= 2 else None)
    
m.add_raw_pose_handler(print)

m.connect()

from adafruit_servokit import ServoKit
skit = ServoKit(channels=16,address=0x41)
num_motor = 6

#Initialize Grasp Control /Serial IO
arduino = serial.Serial('/dev/ttyACM0', 9600, timeout=.1)

val = 2
t = 0.05 #sleep timer
thres = 400

m.run()
val = m.last_pose

def read_sensors():
    fsr1 = 0
    fsr2 = 0
    fsr3 = 0
    fsr4 = 0
    fsr5 = 0
    motor6_d = 0
    motor1 = 0
    motor2 = 0
    motor3 = 0
    motor4 = 0
    motor5 = 0
    
    data = arduino.readline()[:-2] #the last bit gets rid of the new-line chars
    
    if data:
        data = str(data)
        data = data[2:-1]
        raw_list = data.split("#")
        #print(raw_list)
        if len(raw_list) == 11:
            fsr1 = float(raw_list[0])
            fsr2 = float(raw_list[1])
            fsr3 = float(raw_list[2])
            fsr4 = float(raw_list[3])
            fsr5 = float(raw_list[4])
            motor6_d = float(raw_list[5])
            motor1 = float(raw_list[6])
            motor2 = float(raw_list[7])
            motor3 = float(raw_list[8])
            motor4 = float(raw_list[9])
            motor5 = float(raw_list[10])
            
    return [fsr1, fsr2, fsr3, fsr4, fsr5, motor6_d, motor1, motor2, motor3, motor4, motor5]

#Run Main
try:

    while True:

        # Get user input for desired position
        m.run()
        val = m.last_pose

        print('Desired Pose %d' %(m.last_pose))

        # Set motor information
        motor_pos = np.zeros((num_motor-1))
        pos_est = np.zeros((num_motor-1))
        validate_vec = np.array([False,False,False,False,False])
        
        # Read Sensor Data
        sensor_array = read_sensors()

        while(len(sensor_array) < 11):
            sensor_array = read_sensors()

        # Determine Desired Motor pos from desired pose
        if val == 0:
            desired_pos = [0,0,0,0,0]           
        elif val == 1:
            desired_pos = [330,250,250,250,250]    
        elif val == 2:
            desired_pos = [165,125,125,125,125]         
        else:
            continue

        # While motors not at designated point
        while np.count_nonzero(validate_vec) < (num_motor-1):
            
            skit.servo[5].angle = sensor_array[5]

            for i in range(0,num_motor-1):

                # print(i)

                desired_range = list(range(desired_pos[i]-10,desired_pos[i]+10,1))

                # print('Desired_arr Len:')
                # print(len(desired_range))

                #print('Desired_arr:')
                #print(desired_range)

                temp_val = motor_pos[i]

                # Select Speed
                if (temp_val in desired_range): #== True:
                    skit.continuous_servo[i].throttle = 0.1 #Stop
                    validate_vec[i] = True
                else:
                    if motor_pos[i] < desired_pos[i]:
                        scale = 0.0 #-0.1
                    else:
                        scale = 0.2 #0.3

                    # Check if fsr_i has passed threshold
                    if sensor_array[i] > thres:
                        scale = 0.1
                        skit.continuous_servo[i].throttle = scale
                        motor_pos[i] = motor_pos[i] + -1*((scale-0.1)*720*t)
                        print('Position: %d' %(motor_pos[i]))
                        break
                    
                    # If not drive as requested
                    else:

                        skit.continuous_servo[i].throttle = scale
                        motor_pos[i] = motor_pos[i] + -1*((scale-0.1)*720*t)
                        print('Position: %d' %(motor_pos[i]))
                    #true_speed = (scale -0.1)*720 # scale * max 720 deg/s or 120 rpm
                    #skit.continuous_servo[i].throttle = scale
                    #time.sleep(t)
                    #pos_est[i] = motor_pos[i]+true_speed*t

                        #print('Pos_est:')
                        #print(pos_est[i])


            time.sleep(t)

            sensor_array = read_sensors()

            while(len(sensor_array) < 11):
                sensor_array = read_sensors()
                
        print('Valid Count: %d' %(np.count_nonzero(validate_vec)))

            #motor_pos[0:5] = np.array(sensor_array[5:10])
            #err_vec = pos_est - motor_pos

            #print(error_vec)
            #print('\n')

except KeyboardInterrupt:
    print("QUITTING!")
    
    desired_pos =[0,0,0,0,0]
    
    if motor_pos[i] < desired_pos[i]:
            scale = 0.0 #-0.1
    else:
            scale = 0.2 #0.3
    
    n = int(abs(desired_pos[0] - motor_pos[0]) / ((scale-0.1)*720*t))
    print(n)
    
    for j in range(0,n):
        for i in range(0,num_motor-1):
        
            skit.continuous_servo[i].throttle = scale
            motor_pos[i] = motor_pos[i] + -1*((scale-0.1)*720*t)
            print('Position: %d' %(motor_pos[i]))
        
        time.sleep(t)
        
    for i in range(num_motor-1):
        skit.continuous_servo[i].throttle = 0.1
    
    sys.exit(0)
                    
            
    
    
    


